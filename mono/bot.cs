using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

public class Bot
{

    static string botName;
    static string botKey;

    List<List<CarInfo>> carPositionHistory;
    List<CarInfo> current;
    List<CarInfo> last;
    Track track = new Track();
    double deltaDistance = 0;
    Race race = new Race();

    double throttle = 0.5;
    double carAngle;
    Piece nextPiece = new Piece();
    Piece nextNextPiece = new Piece();
    Piece thirdNextPiece = new Piece();
    Piece currentPiece = new Piece();

    MsgTurboAvailable turboInfo;
    bool turboReady;

    double maxSpeed = 6;
    double friction = 1000;
    bool frictionSet = false;
    bool turboOn = false;

    CarInfo myCarInfo;

    int[] optimalLanes;


    //laneswitching
    int currentLane;
    int targetLane;
    bool waitingForLaneSwitch = false;

    public class MsgCarPositions
    {
        public List<CarInfo> data { get; set; }
        public string msgType { get; set; }
        public int gameTick { get; set; }
        public string gameId { get; set; }
    }
    public class CarInfo
    {
        public ID id { get; set; }
        public double angle { get; set; }
        public PiecePosition piecePosition { get; set; }
        public int lap { get; set; }
    }
    public class ID
    {
        public string name { get; set; }
        public string color { get; set; }

    }
    public class PiecePosition
    {
        public int pieceIndex { get; set; }
        public double inPieceDistance { get; set; }
        public Lane lane { get; set; }
        public class Lane
        {
            public int startLaneIndex { get; set; }
            public int endLaneIndex { get; set; }
        }
    }
    public static void Main(string[] args)
    {
        string host = args[0];
        int port = int.Parse(args[1]);
        botName = args[2];
        botKey = args[3];

        Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        using (TcpClient client = new TcpClient(host, port))
        {
            NetworkStream stream = client.GetStream();
            StreamReader reader = new StreamReader(stream);
            StreamWriter writer = new StreamWriter(stream);
            writer.AutoFlush = true;

            new Bot(reader, writer, new Join(botName, botKey));
        }
    }

    private StreamWriter writer;

    //public double DeltaDistance(Track track)
    //{
    //    if (myCarInfo.PiecePosition.pieceIndex == last[0].PiecePosition.pieceIndex)
    //    {
    //        return myCarInfo.PiecePosition.inPieceDistance - last[0].PiecePosition.inPieceDistance;
    //    }
    //    else
    //    {
    //        return track.pieces[last[0].PiecePosition.pieceIndex].length - last[0].PiecePosition.inPieceDistance + myCarInfo.PiecePosition.inPieceDistance;
    //    }
    //}

    void GetCarPositions(Object data)
    {
        Newtonsoft.Json.Linq.JArray tmp = (Newtonsoft.Json.Linq.JArray)data;
        carPositionHistory.Add(tmp.ToObject<List<CarInfo>>());
        current = carPositionHistory[carPositionHistory.Count - 1];

        if (carPositionHistory.Count == 1)
            last = current;
        else
            last = carPositionHistory[carPositionHistory.Count - 2];
        carAngle = current[0].angle;
    }
    void LoppuMikonHaroily()
    {

        foreach (CarInfo car in current)
        {
            if (car.id.name == botName)
            {
                myCarInfo = car;
                currentLane = myCarInfo.piecePosition.lane.startLaneIndex;
            }
        }

        if (myCarInfo.piecePosition.pieceIndex == last[0].piecePosition.pieceIndex)
        {
            deltaDistance = myCarInfo.piecePosition.inPieceDistance - last[0].piecePosition.inPieceDistance;
        }
        else
        {
            //jos pala vaihtuu kesken nii nopeutta ei lasketa ku liian vaikeeta
            deltaDistance = -1;
        }




        //set current and next 2 pieces, avoid outofindex when lap ends
        if (track.pieces.Count - 2 == myCarInfo.piecePosition.pieceIndex)
        {
            nextNextPiece = track.pieces[0];
        }
        else if (track.pieces.Count - 1 == myCarInfo.piecePosition.pieceIndex)
        {
            nextPiece = track.pieces[0];
            nextNextPiece = track.pieces[1];
        }
        else
        {
            nextPiece = track.pieces[myCarInfo.piecePosition.pieceIndex + 1];
            nextNextPiece = track.pieces[myCarInfo.piecePosition.pieceIndex + 2];
        }

        currentPiece = track.pieces[myCarInfo.piecePosition.pieceIndex];

        for (int i = 0; i < track.pieces.Count; ++i)
        {
            if (track.pieces[i].angle / track.pieces[i].radius > 0.44)
            {
                maxSpeed = Math.Abs(track.pieces[i].angle) * track.pieces[i].radius / 750;
                break;
            }
        }

        Console.WriteLine("Speed: " + deltaDistance);
        Console.WriteLine("Angle: " + myCarInfo.angle);
        Console.WriteLine("Current piece angle: " + currentPiece.angle + " Current piece radius: " + currentPiece.radius);
        Console.WriteLine("Next piece angle: " + nextPiece.angle + " Next piece radius: " + nextPiece.radius);


        /* AI PART STARTS HERE */


        if (deltaDistance == -1)
        {
            //dont change throttle if speed wasnt calculated
        }
        else if (!frictionSet)
        {
            //drive to first corner on maxSpeed, when car starts drifting, calculate friction

            if (currentPiece.angle / currentPiece.radius < 0.45)
            {
                if (deltaDistance > maxSpeed)
                {
                    throttle = 0.2;
                }
                else
                {
                    throttle = 1;
                }


            }
            else if (Math.Abs(carAngle) > 0)
            {
                friction = deltaDistance * Math.Abs(currentPiece.angle) / currentPiece.radius;
                frictionSet = true;
                throttle = 0.2;
            }
            else
            {
                throttle = 1;
            }



        }
        else
        {
            //when friction is set, use it to calculate max speeds for next 2 pieces, if those pieces are angled and car is goinng too fast, start slowing down

            if (Math.Abs(currentPiece.angle) > 0 && deltaDistance > friction / (Math.Abs(currentPiece.angle) / currentPiece.radius))
            {
                throttle = 0.1;
            }
            else if (Math.Abs(nextPiece.angle) > 1 && deltaDistance > friction / (Math.Abs(nextPiece.angle) / nextPiece.radius))
            {
                throttle = 0.2;
            }
            else if (Math.Abs(nextNextPiece.angle) > 1 && deltaDistance > friction / (Math.Abs(nextNextPiece.angle) / nextNextPiece.radius))
            {
                throttle = 0.5;

            }
            else if (Math.Abs(thirdNextPiece.angle) > 1 && deltaDistance > (friction / (Math.Abs(thirdNextPiece.angle) / nextNextPiece.radius)) + 2)
            {
                throttle = 0.5;
            }
            else if (Math.Abs(carAngle) > 40)
            {
                throttle = 0.2;
            }
            //if next 2 pieces are straight go full throttle and turbo
            else
            {
                if (turboReady && thirdNextPiece.angle == null)
                {
                    send(new Turbo());
                    Console.WriteLine("turbo");
                }
                throttle = 1;

            }
        }

    }

    //LANESWITCH METHODS
    int GetCurrentEndLane()
    {
        return myCarInfo.piecePosition.lane.endLaneIndex;
    }
    int GetCurrentStartLane()
    {
        return myCarInfo.piecePosition.lane.startLaneIndex;
    }
    bool IsSwitchMsgSent()
    {
        if (waitingForLaneSwitch)
            return true;
        return false;
    }
    string GetNextTurnDirection(int checkStartingDistance)
    {
        int startIndex = myCarInfo.piecePosition.pieceIndex;
        int lastIndex = track.pieces.Count - 1;
        bool switchPossible = false;
        int previousIndex = startIndex + checkStartingDistance - 1;
        for (int i = startIndex + checkStartingDistance; i != startIndex; i++)
        {
            if (i > lastIndex)
            {
                i = 0;
            }

            if (previousIndex < 0)
            {
                previousIndex = lastIndex;
            }

            if (previousIndex < 0)
            {
                previousIndex = lastIndex;
            }

            if (track.pieces[i].laneSwitch)
            {
                switchPossible = true;
            }
            if (switchPossible)
            {
                if (track.pieces[i].angle > 0)
                {

                  if (track.pieces[i].laneSwitch && track.pieces[i].angle > 0 && track.pieces[previousIndex].angle >0)
                       return "Left";
                    return "Right";
                }
                else if (track.pieces[i].angle < 0)
                {

                   if (track.pieces[i].laneSwitch && track.pieces[i].angle < 0 && track.pieces[previousIndex].angle < 0)
                       return "Right";
                    return "Left";
                }
            }
            previousIndex = i;
        }
        return "error";
    }

    void CheckForLaneSwitch()
    {
        if (!IsSwitchMsgSent())
        {
            string tmp = GetNextTurnDirection(1);
            Console.WriteLine("startLane: " + GetCurrentStartLane().ToString() + ", endLane: " + (GetCurrentEndLane().ToString()));
            switch (tmp)
            {
                case "Right":
                    Console.WriteLine("Right: " + currentLane.ToString() + " < " + (track.lanes.Count - 1).ToString());
                    if (currentLane < track.lanes.Count - 1)
                    {
                        send(new SwitchLane(tmp));
                        waitingForLaneSwitch = true;
                    }
                    break;
                case "Left":
                    Console.WriteLine("Left: " + currentLane.ToString() + " < " + "0");
                    if (currentLane > 0)
                    {
                        send(new SwitchLane(tmp));
                        waitingForLaneSwitch = true;
                    }
                    break;
            }
        }
        else if (GetCurrentEndLane() != GetCurrentStartLane())
        {
            waitingForLaneSwitch = false;
            currentLane = GetCurrentEndLane();
        }
    }



    Bot(StreamReader reader, StreamWriter writer, Join join)
    {
        this.writer = writer;
        string line;
        int ticks = 0;
        carPositionHistory = new List<List<CarInfo>>();
        //send(join);
        send(new CreateRace(botName, botKey, "germany", "123gg", 1));
        turboReady = false;

        /*
        for (int i = 0; i < track.pieces.Count; ++i) //++i koska Ossi
        {

            if (track.pieces[i].laneSwitch)
            {
                float tmp;
            }

        }
        */
        while ((line = reader.ReadLine()) != null)
        {

            MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
            switch (msg.msgType)
            {
                case "turboAvailable":
                    turboInfo = JsonConvert.DeserializeObject<MsgTurboAvailable>(line);
                    turboReady = true;
                    break;
                case "carPositions":
                    GetCarPositions(msg.data);
                    MsgCarPositions tmpCarPos = JsonConvert.DeserializeObject<MsgCarPositions>(line);
                    LoppuMikonHaroily();
                    /*
                     * turboInfo.data.
                     * 
                     * turboDurationMilliseconds,
                     * turboDurationTicks,
                     * turboFactor,
                    
                     send(new Turbo());  
                     
                     */

                    //Console.Clear();
                    Console.WriteLine("Throttle: " + throttle);
                    Console.WriteLine("CurrentPieceMaxSpeed: " + (friction / (Math.Abs(currentPiece.angle) / currentPiece.radius)) + " MaxSpeedSet: " + frictionSet + " maxSpeed: " + maxSpeed);
                    if (tmpCarPos.gameTick > 1)
                        CheckForLaneSwitch();
                    send(new Throttle(throttle));

                    break;
                case "join":
                    Console.WriteLine("Joined");
                    send(new Ping());
                    break;
                case "gameInit":
                    MsgInit msgInit = JsonConvert.DeserializeObject<MsgInit>(line);
                    Console.WriteLine(msgInit.data.race.track.name);
                    track = (Track)msgInit.data.race.track;
                    Console.WriteLine("Race init");
                    send(new Ping());
                    break;
                case "gameEnd":
                    Console.WriteLine("Race ended");
                    send(new Ping());
                    break;
                case "gameStart":
                    Console.WriteLine("Race starts");
                    send(new Ping());
                    break;
                case "crash":
                    MsgCrash crash = JsonConvert.DeserializeObject<MsgCrash>(line);
                    if (crash.data.name == botName)
                    {
                        Console.WriteLine("CRASHED");
                    }
                    break;
                case "turboEnd":
                    MsgTurboStatus turboEnds = JsonConvert.DeserializeObject<MsgTurboStatus>(line);
                    if (turboEnds.data.name == botName)
                    {
                        turboOn = false;
                    }
                    break;
                case "turboStart":
                    MsgTurboStatus turboStarts = JsonConvert.DeserializeObject<MsgTurboStatus>(line);
                    if (turboStarts.data.name == botName)
                    {
                        turboOn = true;
                        turboReady = false;
                    }
                    break;
                default:
                    send(new Ping());
                    break;
            }
            Console.WriteLine(++ticks);
        }
        Console.WriteLine("Connection closed. Ticks: " + ticks.ToString());
    }

    private void send(SendMsg msg)
    {
        writer.WriteLine(msg.ToJson());
    }
}

class MsgWrapper
{
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data)
    {
        this.msgType = msgType;
        this.data = data;
    }
}

abstract class SendMsg
{
    public string ToJson()
    {
        return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
    }
    protected virtual Object MsgData()
    {
        return this;
    }

    protected abstract string MsgType();
}

class Join : SendMsg
{
    public string name;
    public string key;
    public string color;

    public Join(string name, string key)
    {
        this.name = name;
        this.key = key;
        this.color = "red";
    }

    protected override string MsgType()
    {
        return "join";
    }
}
class CreateRace : SendMsg
{
    public CreateRaceData data;
    public CreateRace(string name, string key, string trackName, string password, int carCount)
    {
        data = new CreateRaceData(name, key, trackName, password, carCount);
    }
    protected override Object MsgData()
    {
        return this.data;
    }
    protected override string MsgType()
    {
        return "createRace";
    }
}
class CreateRaceData
{
    public CreateRaceData(string name, string key, string trackName, string password, int carCount)
    {
        botId = new BotID();
        botId.name = name;
        botId.key = key;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }
    public BotID botId;
    public string trackName;
    public string password;
    public int carCount;
}

class BotID
{
    public BotID() { }
    public string name;
    public string key;
}
class Ping : SendMsg
{
    protected override string MsgType()
    {
        return "ping";
    }
}

class SwitchLane : SendMsg
{
    public string value;

    public SwitchLane(string value)
    {
        this.value = value;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "switchLane";
    }
}

class Throttle : SendMsg
{
    public double value;

    public Throttle(double value)
    {
        this.value = value;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "throttle";
    }
}
class Turbo : SendMsg
{
    protected override string MsgType()
    {
        return "turbo";
    }
    protected override Object MsgData()
    {
        return "PUUH PUUH PUUH!!";
    }

}

//{"msgType":"turboEnd","data":{"name":"Rosberg","color":"blue"}, "gameTick": 154}

public class MsgTurboStatus
{
    public string msgType { get; set; }
    public int gameTick { get; set; }
    public TurboEndsData data { get; set; }
}
public class TurboEndsData
{
    public string name { get; set; }
    public string color { get; set; }
}
public class MsgCrash
{
    public string msgType { get; set; }
    public CrashData data { get; set; }
    public string gameId { get; set; }
    public int gameTick { get; set; }
}
public class CrashData
{
    public string name { get; set; }
    public string color { get; set; }
}

public class MsgTurboAvailable
{
    public string msgType { get; set; }
    public TurboData data { get; set; }
}
public class TurboData
{
    float turboDurationMilliseconds { get; set; }
    int turboDurationTicks { get; set; }
    float turboFactor { get; set; }
}
public class MsgInit
{
    public string msgType { get; set; }
    public RaceData data { get; set; }
}
public class RaceData
{
    public Race race { get; set; }
}
public class Race
{
    public Track track { get; set; }
    public List<Car> cars { get; set; }
    public RaceSession raceSession { get; set; }
}
public class Track
{

    public StartingPoint startingPoint { get; set; }
    public string id { get; set; }
    public string name { get; set; }
    public List<Piece> pieces { get; set; }
    public List<Lane> lanes { get; set; }
}
public class Piece
{

    public float length { get; set; }
    public float radius { get; set; }
    public float angle { get; set; }
    [JsonProperty("switch")]
    public bool laneSwitch { get; set; }
    //public List<Lane> lanes { get; set; }
}

public class Lane
{
    public float distanceFromCenter { get; set; }
    public int index { get; set; }
}

public class StartingPoint
{
    public Position position { get; set; }

}
public class Position
{
    float x { get; set; }
    float y { get; set; }
}

public class Car
{
    public Dimensions dimensions { get; set; }
    public Id id { get; set; }

}
public class Id
{
    string name { get; set; }
    string color { get; set; }
}

public class Dimensions
{

    float length { get; set; }

    float width { get; set; }

    float guideFlagPosition { get; set; }
}
public class RaceSession
{

    public int laps { get; set; }

    public int maxLapTimeMs { get; set; }

    public bool quickRace { get; set; }
}


